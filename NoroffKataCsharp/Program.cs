﻿using System;

namespace NoroffKataCsharp
{
    class Program
    {
        static void Main(string[] args)
        {
            minPalindromeSteps("race");
            minPalindromeSteps("mada");
            minPalindromeSteps("mirror");
        }
        public static int minPalindromeSteps(string word)
        {
            string temp = word.Substring(0, word.Length);
            temp = Reverse(temp);
            for (int i = temp.Length; i >= 0; i--)
			{
                if(word.Contains(temp.Substring(0, i)))
                {
                    temp = temp.Replace(temp.Substring(0,i), "");
                    i = 0;
                }
			}
            Console.WriteLine(temp);
            Console.WriteLine(temp.Length);
            return temp.Length;
        }
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
